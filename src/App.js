import './App.css';
import { AppStateProvider } from './AppState';
import { AuthProvider } from './Authorization';
import { useAppState } from './AppState';


function App() {

    return (
        <AppStateProvider>
            <AuthProvider>
                <MainComponent/>
            </AuthProvider>
        </AppStateProvider>
    );
}

export default App;

function MainComponent() {
    const { renderCurrentView } = useAppState();
    return renderCurrentView();
}
