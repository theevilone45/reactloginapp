import React, { useState } from 'react';
import { useAuth } from '../src/Authorization';
import { useAppState } from './AppState';


export const ResetPassword = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const { resetPassword } = useAuth();
    const { setCurrentState, AppStateValue } = useAppState();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (resetPassword(email, password)) {
            alert("Zresetowano hasło");
            setCurrentState(AppStateValue.login);
        } else {
            alert("Taki użytkownik nie istnieje");
        }
    }

    return (
        <div className="Window">
            <p>Resetownie hasła</p>
            <form onSubmit={handleSubmit}>
                <input type="email"
                    name="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required></input>
                <input type="password"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required></input>
                <button type="submit" name="reset-password-button" value="Zresetuj hasło">Zresetuj hasło</button>
            </form>
            <button name="registration-button" value="Logowanie" onClick={() => setCurrentState(AppStateValue.login)}>Logowanie</button>
        </div>
    );
}

export default ResetPassword;