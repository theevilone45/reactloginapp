import React, { useState } from 'react';
import { useAuth } from '../src/Authorization';
import { useAppState } from './AppState';

const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const { register } = useAuth();
    const { setCurrentState, AppStateValue } = useAppState();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (register(email, password)) {
            alert("Zarejestrowano");
        } else {
            alert("Użytkownik o podanym mailu już istnieje");
        }
    }

    return (
        <div className="Window">
            <p>Rejestracja</p>
            <form onSubmit={handleSubmit}>
                <input
                    type="email"
                    name="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required>
                </input>
                <input
                    type="password"
                    name="password"
                    placeholder="Pasword"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required>
                </input>
                <button type="submit" name="register-button" value="Zarejestruj się">Zarejestruj się</button>
            </form>
            <button name="login-button" value="Rejestracja" onClick={() => setCurrentState(AppStateValue.login)}>Logowanie</button>
        </div>
    )

}

export default Register;