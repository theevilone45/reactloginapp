import React, { createContext, useState, useContext } from 'react';
import { useAppState } from './AppState';

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [users, setUsers] = useState({});
    const [currentUser, setCurrentUser] = useState(null);

    const {setCurrentState, AppStateValue} = useAppState();

    const login = (email, password) => {
        if (users[email] && users[email].password === password) {
            setCurrentUser({ email });
            return true;
        }
        return false;
    }

    const resetPassword = (email, newPassword) => {
        if (users[email]) {
            setUsers({
                ...users,
                [email]: { ...users[email], password: newPassword },
            });
            return true;
        }
        return false;
    }

    const getCurrentUser = () => {
        return currentUser;
    }

    const logout = () => {
        setCurrentUser(null);
        setCurrentState(AppStateValue.login);
    }

    const register = (email, password) => {
        if (!users[email]) {
            setUsers({
                ...users,
                [email]: { password },
            });
            return true;
        }
        return false;
    };

    return (
        <AuthContext.Provider value={{ login, resetPassword, register, logout, getCurrentUser }}>
            {children}
        </AuthContext.Provider>
    )
};

export const useAuth = () => useContext(AuthContext);