import Register from './Register';
import Login from './Login';
import { createContext, useState, useContext } from 'react';
import { Home } from './Home';
import ResetPassword from './ResetPassword';

const AppStateContext = createContext();

export const AppStateProvider = ({children}) => {
    const AppStateValue = {
        login: 0,
        register: 1,
        resetPassword: 2,
        home: 3
    };

    const initialState = AppStateValue.login;
    const [currentState, setCurrentState] = useState(initialState);


    const renderCurrentView = () => {
        switch (currentState) {
            case AppStateValue.login: return <Login />;
            case AppStateValue.register: return <Register />;
            case AppStateValue.resetPassword: return <ResetPassword/>;
            case AppStateValue.home: return <Home/>
            default: return null;
        }
    }

    return (
        <AppStateContext.Provider value={{setCurrentState, AppStateValue, renderCurrentView}}>
            {children}
        </AppStateContext.Provider>
    )
}

export const useAppState = () => useContext(AppStateContext);