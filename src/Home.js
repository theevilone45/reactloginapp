import { useAuth } from '../src/Authorization';

export const Home = () => {
    const {getCurrentUser, logout} = useAuth();

    const logoutUser = () => {
        logout();
        alert("Wylogowano");
    }
    return (
        <div className="Window">
            <p>Jesteś zalogowany jako: {getCurrentUser().email}</p>
            <button name="logout-button" onClick={() => {logoutUser()}}>Wyloguj</button>
        </div>
    );
}

export default Home;
