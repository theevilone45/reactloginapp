import React, { useState } from 'react';
import { useAuth } from '../src/Authorization';
import { useAppState } from './AppState';


export const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const { login } = useAuth();
    const { setCurrentState, AppStateValue } = useAppState();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (login(email, password)) {
            alert("Zalogowano");
            setCurrentState(AppStateValue.home);
        } else {
            alert("Błędne dane logowania");
        }
    }

    return (
        <div className="Window">
            <p>Logowanie</p>
            <form onSubmit={handleSubmit}>
                <input type="email"
                    name="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required></input>
                <input type="password"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required></input>
                <button type="submit" name="login-button" value="Zaloguj się">Zaloguj się</button>
            </form>
            <button name="registration-button" value="Rejestracja" onClick={() => setCurrentState(AppStateValue.register)}>Rejestracja</button>
            <button name="registration-button" value="Reset hasła" onClick={() => setCurrentState(AppStateValue.resetPassword)}>Reset hasła</button>
        </div>
    );
}

export default Login;